import { Request, Response } from "express";
import { GetAllVideosService } from "../../services/Video/GetAllVideosService";


export class GetAllVideoController {
    async handle(request: Request, response: Response) {
        const service = new GetAllVideosService();

        const videos = await service.execute();

        return response.json(videos);
    }
}