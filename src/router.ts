import { Router } from "express";
import { CreateCategoryController } from "./controllers/cateory/CreateCategoryController";
import { DeleteCategoryController } from "./controllers/cateory/DeleteCategoryController";
import { GetAllCategoriesController } from "./controllers/cateory/GetAllCategoriesController";
import { UpdateCategoryController } from "./controllers/cateory/UpdateCategoryController";
import { CreateVideoController } from "./controllers/video/CreateVideoController";
import { GetAllVideoController } from "./controllers/video/GetAllVideoController";

const routes = Router();

routes.post("/categories", new CreateCategoryController().handle);
routes.get("/categories", new GetAllCategoriesController().handle);
routes.delete("/categories/:id", new DeleteCategoryController().handle);
routes.put("/categories/:id", new UpdateCategoryController().handle);

routes.post('/videos', new CreateVideoController().handle)
routes.get("/videos", new GetAllVideoController().handle);

export { routes }